package tests;

import junit.framework.TestCase;
import main.Ex3;
import main.Ex4;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static junit.framework.Assert.assertTrue;

/**
 * Created by Rannar on 27.09.2015.
 */
public class Ex3Test extends Ex3 {

    private Ex3 e;

    @Before
    public void setUp() {
        e = new Ex3();
    }

    @Test
    public void testOneElement() {
        assertTrue(e.findEvenNumberSum(new int[] {2}) == 2);
    }

    @Test
    public void testThreeElement() {
        assertTrue(e.findEvenNumberSum(new int[] {2, 4, 6}) == 12);
    }

    @Test
    public void testNegElement() {
        assertTrue(e.findEvenNumberSum(new int[] {-2, -4, 2, 4, 6}) == 12);
    }

    @Test
    public void testUnevenElement() {
        assertTrue(e.findEvenNumberSum(new int[] {1, 2, 4, 6}) == 12);
    }

    @Test
    public void testUnevenNegElement() {
        assertTrue(e.findEvenNumberSum(new int[] {-2, -4, 1, 2, 4, 6}) == 12);
    }
}