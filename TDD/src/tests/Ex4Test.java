package tests;

import junit.framework.TestCase;
import main.Ex3;
import main.Ex4;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

import static junit.framework.Assert.assertTrue;

/**
 * Created by Rannar on 27.09.2015.
 */
public class Ex4Test extends Ex3 {

    private Ex4 e;

    @Before
    public void setUp() {
        e = new Ex4();
    }

    @Test
    public void testEmptyArray() throws Exception {
        assertTrue(Arrays.equals(e.sortArray(new int[]{}), (new int[]{})));
    }

    @Test
    public void testOneElemArray() throws Exception {
        assertTrue(Arrays.equals(e.sortArray(new int[] {2}),(new int[] {2})));
    }

    @Test
    public void testTwoElemArray1() throws Exception {
        assertTrue(Arrays.equals(e.sortArray(new int[]{2, 1}),(new int[]{1, 2})));
    }

    @Test
    public void testTwoElemArray2() throws Exception {
        assertTrue(Arrays.equals(e.sortArray(new int[]{2, 3}),(new int[]{2, 3})));
    }

    @Test
    public void testThreeElemArray1() throws Exception {
        assertTrue(Arrays.equals(e.sortArray(new int[]{1, 2, 3}),(new int[]{1, 2, 3})));
    }

    @Test
    public void testThreeElemArray2() throws Exception {
        assertTrue(Arrays.equals(e.sortArray(new int[]{3, 1, 2}),(new int[]{1, 2, 3})));
    }

    @Test
    public void testThreeElemArray3() throws Exception {
        assertTrue(Arrays.equals(e.sortArray(new int[]{3, 2, 1}),(new int[]{1, 2, 3})));
    }

    @Test
    public void testLongArray() throws Exception {
        assertTrue(Arrays.equals(e.sortArray(new int[]{2, 3, 1, 9, 5, 6, 11, 34, 13}),(new int[]{1, 2, 3, 5, 6, 9, 11, 13, 34})));
    }
}