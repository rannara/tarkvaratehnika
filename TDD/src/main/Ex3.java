package main;

/**
 * Created by Rannar on 27.09.2015.
 */
public class Ex3 {

    public int findEvenNumberSum(int[] nums) {
        int sum = 0;

        for (int i = 0; i < nums.length; i++) {
            if (nums[i] > 0 && nums[i] % 2 == 0) {
                sum += nums[i];
            }
        }
        return sum;
    }
}
