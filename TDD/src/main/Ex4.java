package main;

import java.util.Arrays;

/**
 * Created by Rannar on 27.09.2015.
 */
public class Ex4 {

    public int[] sortArray(int[] arr) {

        for (int i = 0; i < arr.length; i++) {
            for (int j = i; j < arr.length; j++) {
                if (arr[j] < arr[i]) {
                    int temp = arr[i];
                    arr[i] = arr[j];
                    arr[j] = temp;
                }
            }
        }
        return arr;
    }

    public static void main(String[] args) {
        Ex4 e = new Ex4();
        System.out.println(Arrays.toString(e.sortArray(new int[]{2, 3, 1, 6, 5})));
    }
}
